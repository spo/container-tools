# Tools container


-------

Docker repo for a container giving you a nice testing sandbox already full of tools to work with.

Contains all the usual useful tools, plus the tools from [Data Science at the Command Line](http://datascienceatthecommandline.com/)

Tools
-  git svn
-  wget curl openssh
-  nodejs python go
-  jq
-  parallel
-  bc
-  xml2json
-  awscli
-  csvkit: 
-    csvcut   csvgrep   csvjson csvpy   csvsql  csvstat    
-    csvclean csvformat csvjoin csvlook csvsort csvstack
-  Data scennce tools
-    Rio       Rio-mds   Rio-pca   Rio-scatter  
-    cols      dseq      header    scrape  weka     weka-cluster
-    arff2csv  csv2arff  dumbplot  pbc     servewd  
-    body      drake     explain   sample  unpack